# User Service API

## Status

Check if service is running
### GET /api

#### Response:

```
{ status: 200, msg:'OK' }
```

## Login

### LinkedIn

### GET /api/login/linkedin

    Client is redirected to LinkedIn login screen

### GET /api/login/linkedin/callback

#### Response:

```
{
    status: Int,
    loginResponse: {
        _id: String,
        __v: Int,
        linkedin: {
            accessToken: String
        }
    }
}
```

### Slack

### GET /api/login/slack

    Client is redirected to slack login screen

### GET /api/login/slack/callback

#### Response:

```
{
    status: Int,
    loginResponse: {
        _id: String,
        __v: Int,
        slack: {
            accessToken: String
        }
    }
}
```

## Users

### GET /api/users/

#### Response:

```
{
    status: Int,
    data: [
        {
            _id: String,
            id: String,
            first_name: String,
            last_name: String,
            __v: Int,
            skills: [String]
        },
        {
            _id: String,
            id: String,
            first_name: String,
            last_name: String,
            __v: Int,
            skills: [String]
        },
        ...
    ]
}
```

### GET /api/users?userId=XXXX

#### Response:

```
{
    status: Int,
    data: {
        _id: String,
        id: String,
        first_name: String,
        last_name: String,
        __v: Int,
        skills: [String]
    }
}
```

### POST /api/users

#### Description:
Adds user

#### Parameters:
```
{
    first_name: String,
    last_name: String,
    id: String (optional)
    skills: [String] (optional
}
```

#### Response:
```
{ status: Int, msg: String}
```

### DELETE /api/users?userId=XXXX

#### Description:
Removes user with :userId

#### Response:
```
{ status: Int, msg: String}
```

## Skills

### GET /api/users/skills?userId=XXXX

#### Description:

Retrieving skills for user

#### Response:

```
[String]
```

### POST /api/users/skills?userId=XXXX

#### Description:

Add skill(s) for user

#### Parameters:

```
{
skills: [String]
}
```

#### Response:
```
{ status: Int, msg: String}
```

### DELETE /api/users/skills?userId=XXXX

#### Description:

Deletes skill(s) for user

#### Parameters:

```
{
skills: [String]
}
```

#### Response:
```
{ status: Int, msg: String}
```

### GET /api/users/skills/linkedin?accessToken=XXXX

#### Description

Retrieves basic LinkedIn profile

#### Response:
```
{
    currentShare: {
        author: {
            firstName: String,
            id: String,
            lastName: String
        },
        comment: String,
        content: {
            description: String,
            eyebrowUrl: String,
            resolvedUrl: String,
            shortenedUrl: String,
            submittedImageUrl: String,
            submittedUrl: String,
            thumbnailUrl: String,
            title: String
        },
        id: String,
        source: {
            serviceProvider: {
                name: String
            }
        },
        timestamp: Int,
        visibility: {
            code: String
        }
    },
    emailAddress: String,
    firstName: String,
    formattedName: String,
    headline: String,
    id: String,
    industry: String,
    lastName: String,
    location: {
        country: {
            code: String
        },
        name: String
    },
    numConnections: Int,
    numConnectionsCapped: [boolean],
    pictureUrl: String,
    pictureUrls: {
        _total: Int,
        values: [
            String
        ]
    },
    positions: {
        _total: Int,
        values: [
            {
                company: {
                    id: Int,
                    industry: String,
                    name: String,
                    size: String,
                    type: String
                },
                id: Int,
                isCurrent: [boolean],
                location: {
                    country: {
                        code: String,
                        name: String
                    },
                    name: String
                },
                startDate: {
                    month: Int,
                    year: Int
                },
                summary: String,
                title: String
                },
                {
                company: {
                    id: Int,
                    industry: String,
                    name: String,
                    size: String,
                    type: String
                },
                id: Int,
                isCurrent: [boolean],
                location: {
                    name: String
                },
                startDate: {
                    year: Int
                },
                summary: String,
                title: String
            }
        ]
    },
    publicProfileUrl: String
}
```



### GET /api/users/skills/slack?userId=XXXX&accessToken=YYYY

#### Response:
```
{
    status: Int,
    data: {
        user: {
            id: String,
            team_id: String,
            name: String,
            deleted: [boolean],
            status: String,
            color: String,
            real_name: String,
            tz: String,
            tz_label: String,
            tz_offset: Int,
            profile: {
                first_name: String,
                last_name: String,
                title: String,
                skype: String,
                phone: String,
                image_Int: String,
                image_Int: String,
                image_Int: String,
                image_Int: String,
                image_Int: String,
                image_Int: String,
                image_Int: String,
                image_original: String,
                avatar_hash: String,
                real_name: String,
                real_name_normalized: String,
                email: String
            },
            is_admin: [boolean],
            is_owner: [boolean],
            is_primary_owner: [boolean],
            is_restricted: [boolean],
            is_ultra_restricted: [boolean],
            is_bot: [boolean],
            has_Intfa: [boolean]
        }
    }
}
```

### GET /api/users/skills/slack/presence?userId=XXXX&accessToken=YYYY

#### Description:

Get presence information for user

#### Response:
```
{
    status: Int,
    data: {
        presence: String,
        online: [boolean],
        auto_away: [boolean],
        manual_away: [boolean],
        connection_count: Int
    }
}
```
### GET /api/users/skills/slack/channels?userId=XXXX&accessToken=YYYY

#### Description:

Retrieve all  channels which a user is connected to

#### Response:
```
{
    status: Int,
    data: [
        {
            name: String,
            id: String,
            topic: {
                value: String,
                creator: String,
                last_set: Int
            },
            purpose: {
            value: String,
            creator: String,
            last_set: Int
        }
        },
        {
            name: String,
            id: String,
            topic: {
                value: String,
                creator: String,
                last_set: Int
            },
            purpose: {
                value: String,
                creator: String,
                last_set: Int
            }
        }
    ]
}
```

### GET /api/users/skills/slack/stars?userId=XXXX&accessToken=YYYY

#### Description:

Get items which user has stared

#### Response:

```
{
    status: Int,
    data: {
        items: [
            {
                type: String,
                channel: String,
                message: {
                    type: String,
                    user: String,
                    text: String,
                    ts: String,
                    permalink: String,
                    is_starred: [boolean]
                }
            }
        ],
        paging: {
            count: Int,
            total: Int,
            page: Int,
            pages: Int
        }
    }
}
```
