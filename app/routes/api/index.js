
import express from 'express';
import login from './login';
import users from './users';

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
    var router = express.Router();

    router.use('/login', login(app, dbService));
    router.use('/users', users(app, dbService));
    router.get('/', (req, res)=> {
        res.json({ status: 200, msg: 'OK' });
    });

    return router;
};
