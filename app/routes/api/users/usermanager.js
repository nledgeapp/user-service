import _ from 'lodash';
import path from 'path';
import uuid from 'uuid';

let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config')),
    dbModels = null;

export default class UserManager {

    constructor(params) {
        this.dbService = params.dbService;
        dbModels = this.dbService.getDbModels();
        this.User = dbModels.User;
    }

    getAllUsers() {
        return new Promise((resolve,reject)=>{
            let query = {};
            this.User.find(query, (err, data)=> {
                if (err) {
                    reject({status: 500, msg: 'Unable to retrieve users: '+err});
                } else if (data.length) {
                    // We found a user
                    resolve({status: 200, data: data});
                } else {
                    // No user was found
                    resolve({status: 404, msg: 'No users were found'});
                }
            });
        });
    }

    findUserById(userId) {
        return new Promise((resolve,reject)=>{
            if (!userId) {
                return reject({status: 400, msg: 'Unable to retrieve user (userId is missing)'});
            }
            let query = { id:userId };
            this.User.find(query, (err, data)=> {
                if (err) {
                    reject({status: 500, msg: 'Unable to retrieve user: '+err});
                } else if (data.length > 1) {
                    reject({status: 500, msg: 'Id did not return unique user'});
                } else if (data.length) {
                    // We found a user
                    resolve({status: 200, data: data[0]});
                } else {
                    // No user was found
                    resolve({status: 404, msg: 'No user was found'});
                }
          	});
        });
    };

    delete(userId) {
        return new Promise((resolve,reject)=>{
            if (!userId) {
                return reject({status: 400, msg: 'Unable to delete user (userId is missing)'});
            }
            let query = { id:userId };
            this.User.remove(query, (err, data)=> {
                if (err) {
                    reject({status: 500, msg: 'Unable to remove user: '+err});
                } else {
                    resolve({status: 200, msg: 'User was removed'});
                }
            });
        });
    }

    addUser(userData) {
        return new Promise((resolve,reject)=>{
            if (!userData) {
                return reject({status: 400, msg:'Unable to add user [addUser() is missing user data]'})
            }
            if (!userData.first_name) {
                return reject({status: 400, msg:'Unable to add user [addUser() is missing first_name]'})
            }
            if (!userData.last_name) {
                return reject({status: 400, msg:'Unable to add user [addUser() is missing last_name]'})
            }
            if (!userData.id) {
                userData.id = uuid.v4();
            }
            let user = new this.User(userData);
            user.save((err, data)=>{
                if (err) {
                    reject({status:500, msg:'Unable to create db entry from Linkedin data: '+err});
                } else {
                    resolve({status:200, data:data});
                }
            });
        });
    };

    addSkills(userId, skills) {
      return new Promise((resolve, reject)=>{
          if (!userId) {
              return reject({status: 400, msg: 'Unable to add skills (userId is missing)'});
          }
          if (!skills) {
              return reject({status: 400, msg: 'Unable to add skills (skills are missing)'});
          }
          this.findUserById(userId)
          .then((resp)=> {
              let user = new this.User(resp.data),
                  userSkills = resp.data.skills;

              skills.forEach( (skill) => {
                  if (!_.includes(userSkills, skill.toLowerCase())) {
                      userSkills.push(skill.toLowerCase());
                  }
              });
              user.update({skills:userSkills}, (err, data)=> {
                  if (err) {
                    reject({status: 500, msg:err});
                } else {
                    resolve({status:200, msg:data});
                }
              });
          })
          .catch((err)=> {
              reject(err);
          })
      });
    }

    deleteSkills(userId, skills) {
      return new Promise((resolve,reject)=>{
          if (!userId) {
              return reject({status: 400, msg: 'Unable to add skills (userId is missing)'});
          }
          if (!skills) {
              return reject({status: 400, msg: 'Unable to add skills (skills are missing)'});
          }
          this.findUserById(userId)
          .then((resp)=> {
              let user = new this.User(resp.data),
                  userSkills = resp.data.skills;

              _.remove(userSkills, (skill)=> {
                  return _.includes(skills, skill);
              });

              user.update({skills:userSkills}, (err, data)=> {
                if (err) {
                    reject({status: 500, msg:err});
                } else {
                    resolve({status:200, msg:data});
                }
              });
          })
          .catch((err)=> {
              reject(err);
          })
      });
    }
}
