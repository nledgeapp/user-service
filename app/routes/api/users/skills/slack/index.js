'use strict';
import slack from 'slack';
import express from 'express';
import SlackSkillsController from './slack.controller';

export default (app, dbService) => {
    let controller = new SlackSkillsController({slack}),
        router = express.Router();

    router.get('/', (req, res)=> {
        let accessToken = req.query.accessToken,
            userId = req.query.userId;

        controller.getUser({accessToken, userId})
        .then( (resp)=> {
            res.json({status:200,data:resp});
            res.end();
        })
        .catch( (err)=> {
            res.json(err);
            res.end();
        });
    });

    router.get('/presence', (req, res)=> {
        let accessToken = req.query.accessToken,
            userId = req.query.userId;

        controller.getPresence({accessToken, userId})
        .then( (resp)=> {
            res.json({status:200,data:resp});
            res.end();
        })
        .catch( (err)=> {
            res.json(err);
            res.end();
        });
    });

    router.get('/channels', (req, res)=> {
        let accessToken = req.query.accessToken,
            userId = req.query.userId;

        controller.getUsersChannels({accessToken, userId})
        .then( (resp)=> {
            res.json({status:200,data:resp});
            res.end();
        })
        .catch( (err)=> {
            res.json(err);
            res.end();
        });
    });

    router.get('/stars', (req, res)=> {
        let accessToken = req.query.accessToken,
            userId = req.query.userId;

        controller.getStaredItems({accessToken, userId})
        .then( (resp)=> {
            res.json({status:200,data:resp});
            res.end();
        })
        .catch( (err)=> {
            res.json(err);
            res.end();
        });
    });

    return router;
};
