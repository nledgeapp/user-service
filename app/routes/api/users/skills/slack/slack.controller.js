'use strict';

export default class SlackSkillsController {
    constructor(params) {
        this.slack = params.slack;
    };

    getUser({accessToken, userId}){
        return new Promise((resolve,reject)=>{
            return this.getUserData({token:accessToken,user:userId})
            .then( (resp)=> {
                resolve({status:200, data:resp});
            })
            .catch( (err)=> {
                reject(err);
            });
        });
    }

    getPresence({accessToken, userId}){
        return new Promise((resolve,reject)=>{
            return this.getUserPresence({token:accessToken,user:userId})
            .then((resp)=>{
                resolve({status:200,data:resp});
            })
            .catch((err)=>{
                reject(err);
            });
        });
    }

    getUsersChannels({accessToken, userId}){
        return new Promise((resolve,reject)=> {
            return this.listChannels({
                token:accessToken,
                exclude_archived:true
            })
            .then( (resp)=> {
                return this.filterChannelsOnUser(resp.channels,userId);
            })
            .then( (resp)=> {
                resolve({status:200,data:resp});
            })
            .catch((err)=>{
                reject(err);
            });
        });
    }

    getStaredItems({accessToken, userId}){
        return new Promise((resolve,reject)=> {
            return this.listStaredItems({
                token: accessToken,
                user: userId
            })
            .then( (resp)=> {
                resolve({status:200,data:resp});
            })
            .catch( (err)=> {
                reject(err);
            });
        });
    }

    listStaredItems(params){
        return new Promise((resolve,reject)=>{
            this.slack.stars.list(params, (err, data)=>{
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    listChannels(params){
        return new Promise((resolve,reject)=>{
            this.slack.channels.list(params, (err, data)=>{
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    filterChannelsOnUser(channelList,userId){
        let userChannels = [];
      	for (let channel of channelList) {
            if (channel.members.indexOf(userId) >= 0) {
                userChannels.push({
                    name:channel.name,
                    id:channel.id,
                    topic:channel.topic,
                    purpose:channel.purpose
                });
            }
        }
        return userChannels;
    }

    getUserPresence(params){
      	return new Promise((resolve,reject)=>{
      		  this.slack.users.getPresence(params, (err, data)=>{
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
      	});
    }

    getUserData(params){
        return new Promise((resolve,reject)=>{
        		this.slack.users.info(params, (err, data)=>{
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
    	  });
    }
}
