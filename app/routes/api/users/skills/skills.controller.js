'use strict';
import _ from 'lodash';
import path from 'path';
import url from 'url'

export default class SkillsController {
    constructor(params) {
        this.userManager = params.userManager;
    };

    get(userId) {
        return new Promise((resolve, reject)=> {
            if (!userId) {
              return reject({status: 400, msg: 'Unable to get skills (userId is missing)'});
            }
            this.userManager.findUserById(userId)
            .then( (resp)=> {
                resolve(resp.data.skills);
            })
            .catch( (err)=> {
                reject(err);
            });
        });
    }

    post({userId, skills}) {
      return new Promise((resolve, reject)=> {
          if (!userId) {
            return reject({status: 400, msg: 'Unable to add skills (userId is missing)'});
          }
          if (!skills) {
            reject({status:400, msg: 'Unable to add skills (skills are missing)'});
          }
          this.userManager.addSkills(userId, skills)
          .then( ()=> {
              resolve({status: 200, msg: 'Skills added'});
          })
          .catch( (err)=> {
              reject(err);
          });
      });
    }

    delete({userId, skills}) {
      return new Promise((resolve, reject)=> {
          if (!userId) {
            return reject({status: 400, msg: 'Unable to add skills (userId is missing)'});
          }
          if (!skills) {
            reject({status:400, msg: 'Unable to add skills (skills are missing)'});
          }
          this.userManager.deleteSkills(userId, skills)
          .then( (resp)=> {
              resolve({status: 200, msg: 'Skills removed'});
          })
          .catch( (err)=> {
              reject(err);
          });
      });
    }
}
