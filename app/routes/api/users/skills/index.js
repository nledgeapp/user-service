import _ from 'lodash';
import express from 'express';
import linkedin from './linkedin';
import slack from './slack';
import UserManager from '../usermanager';
import SkillsController from './skills.controller';

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
  let router = express.Router(),
      userManager = new UserManager({dbService}),
      skillsController = new SkillsController({userManager});

  router.use('/linkedin', linkedin(app, dbService));
  router.use('/slack', slack(app, dbService));
  router.get('/', (req, res)=> {
      let queryParams = req.query;
      skillsController.get(queryParams.userId)
      .then( (resp)=> {
          res.json(resp);
          res.end()
      })
      .catch( (err)=> {
          res.json({err});
          res.end();
      });
  });

  router.post('/', (req, res)=> {
      let queryParams = req.query;
      let requestData = req.body;
      skillsController.post(_.merge(requestData, queryParams))
      .then( (resp)=> {
          res.json(resp);
          res.end()
      })
      .catch( (err)=> {
          res.json({err});
          res.end();
      });
  });

  router.delete('/', (req, res)=> {
      let queryParams = req.query;
      let requestData = req.body;
      console.log(_.merge(requestData, queryParams));
      skillsController.delete(_.merge(requestData, queryParams))
      .then( (resp)=> {
          res.json(resp);
          res.end()
      })
      .catch( (err)=> {
          res.json({err});
          res.end();
      });
  });

  return router;
};
