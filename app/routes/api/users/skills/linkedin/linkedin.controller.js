'use strict';
import _ from 'lodash';
import url from 'url'

export default class LinkedInSkillsController {

    constructor(params) {
        this.linkedinSevice = params.linkedinSevice;
    }

    get({accessToken} = {}){
        return new Promise((resolve,reject)=>{
        	if (!accessToken) {
                reject({status:400, msg:'LinkedInSkillsController.get() accessToken is missing'});
        	} else {
        		return this.getLinkedinSkills(accessToken)
        		.then( (resp)=> {
                    resolve(resp);
        		})
                .catch( (err)=> {
                    reject(err);
                });
        	}
        });
    };

    getLinkedinSkills(accessToken) {
    	return new Promise((resolve,reject)=> {
    		let linkedIn = this.linkedinSevice.getInstance(accessToken);

    		if (linkedIn) {
    			linkedIn.people.me( (err,data)=> {
    				if (err) {
    					return reject(err);
                    }
    				resolve(data);
    			});
    		} else {
    			reject({status:400,msg:'LinkedIn instance is missing'})
    		}
    	});
    }
}
