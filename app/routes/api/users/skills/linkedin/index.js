'use strict';
import path from 'path';
import express from 'express';
import LinkedInSkillsController from './linkedin.controller';

let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config'));

export default (app, dbService) => {
    let linkedinSevice = require(path.resolve(root,'services/linkedin-service')),
        controller = new LinkedInSkillsController({linkedinSevice}),
        router = express.Router();

    router.get('/', (req, res)=> {
        let accessToken = req.query.accessToken;

        controller.get({accessToken})
        .then( (resp)=> {
            res.json(resp);
            res.end();
        })
        .catch( (err)=> {
            res.json(err);
            res.end();
        });
    });

    return router;
};
