import express from 'express';
import skills from './skills';
import UserManager from './usermanager';

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
    let router = express.Router(),
        userManager = new UserManager({dbService});

    router.use('/skills', skills(app, dbService));

    router.get('/', (req, res)=> {
        let userId = req.query.userId;
        if (userId) {
            userManager.findUserById(userId)
            .then( (resp)=> {
              res.json(resp);
              res.end();
            })
            .catch( (err)=> {
              res.json({err});
              res.end();
            });
        } else {
            userManager.getAllUsers()
            .then((resp)=> {
              res.json(resp);
              res.end()
            })
            .catch((err)=> {
              res.json({err});
              res.end();
            });
        }
    });

    router.post('/', (req, res)=> {
      let userData = req.body;
      userManager.addUser(userData)
      .then((resp)=> {
        res.json(resp);
        res.end();
      })
      .catch((err)=> {
        res.json({err});
        res.end();
      });
    });

    router.delete('/', (req, res)=> {
        let userId = req.query.userId;

        userManager.delete(userId)
          .then( (resp)=> {
              res.json(resp);
              res.end()
          })
          .catch( (err)=> {
              res.json({err});
              res.end();
          });
    });
    return router;
};
