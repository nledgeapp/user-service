'use strict';
import _ from 'lodash';
import url from 'url';
import path from 'path';
import http from 'http';
import https from 'https';
import querystring from 'querystring';
let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config'));

export default class LinkedInLoginController {

    constructor(params){
        this.userManager = params.userManager;
        this.sa = params.superagent
    }

    getLoginUrl() {
    	let loginUrl = url.format({
    		protocol:'https',
    		host:config.authConfig.linkedin.host,
    		pathname: config.authConfig.linkedin.loginpath,
    		search: querystring.stringify({
    			client_id:config.authConfig.linkedin.clientId,
    			redirect_uri:config.authConfig.linkedin.callbackUrl,
    			state:config.authConfig.linkedin.state,
    			scope:config.authConfig.linkedin.permissions,
    			response_type:'code'
    		})
    	});
        return loginUrl;
    };

    processLinkedInResponse({code, state} = {}) {
        return new Promise((resolve,reject)=>{

        	let redirectUri = config.authConfig.linkedin.callbackUrl,
        		clientId = config.authConfig.linkedin.clientId,
        		clientSecret = config.authConfig.linkedin.clientSecret,
        		url = config.authConfig.linkedin.accessTokenUrl,
        		body = querystring.stringify({
        			client_id:config.authConfig.linkedin.clientId,
        			client_secret:config.authConfig.linkedin.clientSecret,
        			redirect_uri:config.authConfig.linkedin.callbackUrl,
        			code:code,
        			grant_type:'authorization_code'
        		}),
        		linkedInData = null,
        		accessToken = null;

        	if (state !== config.authConfig.linkedin.state) {
                return reject({status:500, msg:'CRSV failed'});
        	}

        	return this.getAccessToken(body)
            .then( (resp)=> {
              accessToken = resp.data.body.access_token;
              return this.getLinkedinData(accessToken);
            })
            .then( (resp)=> {
              linkedInData = resp.data.body;
              let linkedInId = linkedInData.id;
              return this.findUserInDb(linkedInId);
            })
            .then( (resp)=> {
              if (resp.status == 404) {
                return this.createUserInDb(linkedInData);
              } else {
                return resp;
              }
            })
            .then( (resp)=> {
              // Mongoose objects are sealed, by transforming the response with toObject()
              // we can add more attributes to the object
              let loginResponse = resp.data.toObject();
              loginResponse.linkedIn = { accessToken: accessToken };

              resolve({status: 200, data: loginResponse, linkedInData});
            })
            .catch( (err)=> {
                  reject(err);
            });
        });
    };

    getAccessToken(body){
    	return new Promise((resolve,reject)=>{
    		return this.sa.post(config.authConfig.linkedin.accessTokenUrl)
    		.send(body)
    		.set('Content-Type', 'application/x-www-form-urlencoded')
    		.end( (err, resp)=> {
    			if (err || !resp.ok) {
    				reject({status:500,msg:"Unable to retrieve linkedin accessToken: " + err});
    			} else {
    				resolve({data:resp});
    			}
    		});
    	});
    }

    getLinkedinData(accessToken){
    	return new Promise((resolve,reject)=>{
    		// Initiate linkedin services
    		return this.sa.get(config.authConfig.linkedin.peopleUrl + "?format=json")
          .set('Authorization', 'Bearer ' + accessToken)
          .end( (err, resp)=> {
            if (err || !resp.ok) {
              reject({status:500,msg:"Unable to retrieve linkedin data: " + err});
            } else {
              resolve({status:200,data:resp});
            }
          });
    	});
    }

    findUserInDb(id){
      return this.userManager.findUserById(id,'slack');
    }

    createUserInDb(userData){
    	// create user if not exists
    		return this.userManager.addUser({
					providerIds: [{ provider: 'linkedIn', userId: userData.id }],
					first_name: userData.firstName,
					last_name: userData.lastName,
          email: userData.emailAddress
    		});
    }
}
