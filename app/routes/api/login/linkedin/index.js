'use strict';
import express from 'express';
import UserManager from '../../users/usermanager';
import superagent from 'superagent';
import LinkedInLoginController from './linkedin.controller';

export default (app, dbService) => {
    let userManager = new UserManager({dbService}),
        controller = new LinkedInLoginController({superagent, userManager}),
        router = express.Router();

    router.get('/', (req, res)=> {
        res.redirect(controller.getLoginUrl());
    });

    router.get('/callback', (req, res)=> {
        let code = req.query.code,
    		state = req.query.state;

        controller.processLinkedInResponse({ code, state })
          .then( (resp)=> {
              res.json(resp);
              res.end();
          })
          .catch( (err)=> {
              res.json(err);
              res.end();
          });
    });

    return router;
};
