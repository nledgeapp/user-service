
import express from 'express';
import linkedin from './linkedin';
import slack from './slack';

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
    let router = express.Router();

    router.use('/linkedin',linkedin(app, dbService));
    router.use('/slack', slack(app, dbService));

    return router;
};
