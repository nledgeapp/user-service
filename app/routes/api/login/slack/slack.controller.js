'use strict';
import _ from 'lodash';
import path from 'path';
import http from 'http';
import https from 'https'
import url from 'url';
import querystring from 'querystring';
let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config'));

export default class SlackLoginController {

    constructor(params) {
        this.sa = params.superagent;
        this.slack = params.slack;
        this.userManager = params.userManager;
    };

    getLoginUrl() {
    	let loginUrl = url.format({
    		protocol:'https',
    		host:config.authConfig.slack.host,
    		pathname: config.authConfig.slack.loginpath,
    		search: querystring.stringify({
    			client_id:config.authConfig.slack.clientId,
                scope:config.authConfig.slack.permissions,
    			redirect_uri:config.authConfig.slack.callbackUrl,
    			state:config.authConfig.slack.state
    		})
    	});
    	return loginUrl;
    };

    processSlackResponse({code, state} = {}) {
        return new Promise((resolve,reject)=>{

        	let redirectUri = config.authConfig.slack.callbackUrl,
        		clientId = config.authConfig.slack.clientId,
        		clientSecret = config.authConfig.slack.clientSecret,
        		url = config.authConfig.slack.accessTokenUrl,
        		body = querystring.stringify({
        			client_id:config.authConfig.slack.clientId,
        			client_secret:config.authConfig.slack.clientSecret,
        			redirect_uri:config.authConfig.slack.callbackUrl,
        			code:code
        		}),
        		slackData = null,
        		accessToken = null;

        	if (state !== config.authConfig.slack.state) {
                return reject({status:500, msg:'CRSV failed'});
        	}

        	return this.getAccessToken(body)
        	.then( (resp)=> {
                let respText = JSON.parse(resp.data.text),
        		      userId = respText.user_id;

        		accessToken = respText.access_token;
        		return this.getSlackUserData({accessToken,userId});
        	})
        	.then( (resp)=> {
        		slackData = resp.data;
        		return this.findUserInDb(slackData.user.id);
        	})
        	.then( (resp)=> {
        		if (resp.status == 404) {
        			return this.createUserInDb(slackData.user);
        		} else {
        			return resp;
        		}
        	})
        	.then( (resp)=> {
        		// Mongoose objects are sealed, by transforming the response with toObject()
        		// we can add more attributes to the object
        		let loginResponse = resp.data.toObject();
        		loginResponse.slack = {accessToken: accessToken};
						resolve({status:200, data: loginResponse, slackData});
        	})
        	.catch( (err)=> {
                reject(err);
        	});
        });
    };

    getAccessToken(body) {
    	return new Promise((resolve,reject)=>{
    		this.sa.post(config.authConfig.slack.accessTokenUrl)
    		.send(body)
    		.set('Content-Type', 'application/x-www-form-urlencoded')
    		.end( (err, resp)=> {
    			if (err || !resp.ok) {
    				reject({status:500,msg:"Unable to retrieve slack accessToken: " + err});
    			} else {
    				resolve({data:resp});
    			}
    		});
    	});
    };

    getSlackUserData(params){
    	return new Promise((resolve,reject)=>{
    		this.slack.users.info({token:params.accessToken, user:params.userId}, (err, data)=>{
						if (err) {
								return reject({status:500,msg:'unable to retrieve slack data: '+err});
						}
						resolve({status:200,data:data});
					});
    	});
    };

    findUserInDb(id){
    	return new Promise( (resolve,reject)=> {
    		this.userManager.findUserById(id,'slack')
    		.then( (data)=> {
                resolve(data);
            })
    		.catch( (err)=> {
                reject(err);
            });
    	});
    };

    createUserInDb(userData){
    	return new Promise((resolve,reject)=>{
    		this.userManager.addUser({
          providerIds: [{ provider: 'slack', userId: userData.id }],
    			first_name:userData.profile.first_name,
    			last_name:userData.profile.last_name,
					email:userData.profile.email
    		})
    		.then( (data)=> {
                resolve(data);
            })
    		.catch( (err)=> {
                reject(err);
            });
    	});
    };
}
