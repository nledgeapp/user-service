'use strict';
import slack from 'slack';
import express from 'express';
import superagent from 'superagent';
import UserManager from '../../users/usermanager';
import SlackLoginController from './slack.controller';

export default (app, dbService) => {
    let userManager = new UserManager({dbService}),
        controller = new SlackLoginController({superagent, slack, userManager}),
        router = express.Router();

    router.get('/', (req, res)=> {
        res.redirect(controller.getLoginUrl());
    });

    router.get('/callback', (req, res)=> {
        let code = req.query.code,
    		state = req.query.state;

        controller.processSlackResponse({code, state})
        .then( (resp)=> {
            res.json(resp)
            res.end()
        })
        .catch( (err)=> {
            res.json({err});
            res.end();
        });
    });
    return router;
};
