'use strict';

import root from './root.route.js';
import api from './api';
import errors from './errors.route.js';

export default (app, dbService)=> {
  // Add routes for app
  app.get('/', root);

  // API routes
  app.use('/api', api(app, dbService));
  app.get('*', (req, res)=> {
    res.json({status:404, msg:'Route does not exist'});
  });
}
