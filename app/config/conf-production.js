'use strict';

/*********************************
  Add production specific config
  This will override the common/all one
**********************************/
export default {
    mongo:{
        uri: 'mongodb://ds013310.mlab.com:13310/heroku_0mtww01j',
        options:{
            user: 'user-service-dbuser',
            pass: 'userservicepw1'
        }
    }
};
