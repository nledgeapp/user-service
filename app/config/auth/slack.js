export default {
    development: {
        appName: "edge-app",
        host: "www.slack.com",
        loginpath: "/oauth/authorize",
        accessTokenUrl: "https://www.slack.com/api/oauth.access",
        accessTokenPath:"/api/oauth.access",
        clientId: "2172920488.41894223664",
        clientSecret: "396efa3c46a0494c915237a38d2ab654",
        callbackUrl: "http://localhost:9000/api/login/slack/callback", // ensure it's up to date with LinkedIn settings
        state: "DCEeFWf45A53sdfKef424",
        permissions: "channels:read groups:read users:read usergroups:read team:read stars:read"
    },
    production: {
        appName: "edge-app",
        host: "www.slack.com",
        loginpath: "/oauth/authorize",
        accessTokenUrl: "https://www.slack.com/api/oauth.access",
        accessTokenPath:"/api/oauth.access",
        clientId: "2172920488.41894223664",
        clientSecret: "396efa3c46a0494c915237a38d2ab654",
        callbackUrl: "https://edge-summit-user-service.herokuapp.com/api/login/slack/callback", // ensure it's up to date with LinkedIn settings
        state: "DCEeFWf45A53sdfKef424",
        permissions: "channels:read groups:read users:read usergroups:read team:read stars:read"
    }
}
