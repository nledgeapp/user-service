'use strict';
import * as slack from './slack';
import * as linkedin from './linkedin';


var ENV_AUTH = {};

switch (process.env.NODE_ENV || 'development') {
  case 'production':
    ENV_AUTH.slack = slack.production;
    ENV_AUTH.linkedin = linkedin.production;
    break;
  default:
      ENV_AUTH.slack = slack.development;
      ENV_AUTH.linkedin = linkedin.development;
    break;
}

export default ENV_AUTH;
