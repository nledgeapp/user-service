export default {
    development:{
        appName: "edge-app",
        host: "www.linkedin.com",
        loginpath: "/uas/oauth2/authorization",
        accessTokenUrl: "https://www.linkedin.com/uas/oauth2/accessToken",
        accessTokenPath:"/uas/oauth2/accessToken",
        clientId: "77pdvjfz3hkhsw",
        clientSecret: "tIkFVlKuCSbjfJjw",
        callbackUrl: "http://localhost:9000/api/login/linkedin/callback", // ensure it's up to date with LinkedIn settings
        state: "DCEeFWf45A53sdfKef424",
        permissions: "r_basicprofile,r_emailaddress",
        peopleUrl: "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,email-address)"
    },
    production:{
        appName: "edge-app",
        host: "www.linkedin.com",
        loginpath: "/uas/oauth2/authorization",
        accessTokenUrl: "https://www.linkedin.com/uas/oauth2/accessToken",
        accessTokenPath:"/uas/oauth2/accessToken",
        clientId: "77pdvjfz3hkhsw",
        clientSecret: "tIkFVlKuCSbjfJjw",
        callbackUrl: "https://edge-summit-user-service.herokuapp.com/api/login/linkedin/callback", // ensure it's up to date with LinkedIn settings
        state: "DCEeFWf45A53sdfKef424",
        permissions: "r_basicprofile,r_emailaddress",
        peopleUrl: "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,email-address)"
    }
};
