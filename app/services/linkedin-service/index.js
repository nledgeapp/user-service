import path from 'path'
var root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config')),
    LinkedIn = require('node-linkedin')(
        config.authConfig.linkedin.appName,
        config.authConfig.linkedin.clientSecret,
        config.authConfig.linkedin.callbackUrl
    )

var getInstance = (accessToken) => {
    return LinkedIn.init(accessToken);
}

export default{
    getInstance
}
