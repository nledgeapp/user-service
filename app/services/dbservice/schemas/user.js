import mongoose from 'mongoose';
let Schema = mongoose.Schema;

var ProviderSchema = new Schema({
    provider: String,
    userId: String
  },
  { _id: false }
);

export default new Schema({
    providerIds: [ProviderSchema],
    first_name: String,
    last_name: String,
    email: String,
    linkedIn: Schema.Types.Mixed,
    slack: Schema.Types.Mixed,
    skills: [Schema.Types.Mixed]
  },
  {collection: 'users'}
)
