import path from 'path'
import mongoose from 'mongoose';
import userSchema from './schemas/user';

mongoose.Promise = global.Promise;

let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config')),
    dbModels = {};

export default class DatabaseService {

    constructor() {}

    connectDb() {
        return new Promise((resolve, reject)=>{
            mongoose.connect(config.mongo.uri, config.mongo.options)
            .then((resp)=>{
                this.configureDbModels();
                resolve(mongoose);
            })
            .catch((err)=>{
                reject(err);
            });
        });
    }

    configureDbModels() {
        dbModels.User = mongoose.model('User', userSchema);
    }

    getDbModels() {
        return dbModels;
    }
}
